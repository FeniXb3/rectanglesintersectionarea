﻿using System;

namespace RectanglesIntersectionArea
{
    public class Solution
    {
        public int solution(int blx1, int bly1, int trx1, int try1, int blx2, int bly2, int trx2, int try2)
        {
            // −2 147 483 648 <= ARGS <= 2 147 483 647
            // time: O(1)
            // space: O(1)
            // return:
            //      intersection area if exists
            //      0 if no intersection
            //      -1 if area exceeds 2 147 483 647

            if (blx1 >= trx2 || blx2 >= trx1 || bly1 >= try2 || bly2 >= try1)
                return 0;

            long area = 0;

            long newblx = Math.Max(blx1, blx2);
            long newbly = Math.Max(bly1, bly2);
            long newtrx = Math.Min(trx1, trx2);
            long newtry = Math.Min(try1, try2);

            try
            {
                area = (newtrx - newblx) * (newtry - newbly);

                if (area > int.MaxValue)
                    return -1;
            }
            catch (System.OverflowException)
            {
                return -1;
            }

            return (int)area;
        }

    }
}
