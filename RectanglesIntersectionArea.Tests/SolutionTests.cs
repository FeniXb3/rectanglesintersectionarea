﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RectanglesIntersectionArea.Tests
{
    [TestClass]
    public class SolutionTests
    {
        // −2 147 483 648 <= ARGS <= 2 147 483 647
        // time: O(1)
        // space: O(1)
        // return:
        //      intersection area if exists
        //      0 if no intersection
        //      -1 if area exceeds 2 147 483 647

        [TestMethod]
        public void TestSolutionExample()
        {
            var excersice = new Solution();

            int expected = 16;

            int result = excersice.solution(0, 2, 5, 10, 3, 1, 20, 15);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSolutionNoArea()
        {
            var excersice = new Solution();

            int expected = 0;

            int result = excersice.solution(-5, -5, 0, 0, 0, 0, 5, 5);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSolutionExtreme()
        {
            var excersice = new Solution();

            int expected = -1;

            int result = excersice.solution(Int32.MinValue, Int32.MinValue, Int32.MaxValue, Int32.MaxValue, Int32.MinValue, Int32.MinValue, Int32.MaxValue, Int32.MaxValue);

            Assert.AreEqual(expected, result);
        }
    }
}
